import api from '../../config/api'

const state = {
	upload: {
		progress: false,
		success: false,
		error: 'Message'
	},
	selectedFile: null,
	fileUploaded: [],
	unitsFilter: {},
	units: {
		results: [],
		paging: {
			page: 0,
			limit: 0,
			total: 0
		}
	},
	search: {
    owner: null,
    apartment: null,
    tower: null,
    floor: null,
    unit: null,
    bed: null,
    bath: null,
    size: null,
    status: 'active,rented',
    page: 1
	},
	searchOptions: {
		status: ['active','rented']
	}
}

const getters = {
	units: (state, getters) => {
		return state.units
	}
}

const actions = {
	UPDATE_UNIT({commit, state}, data){
		return new Promise((resolve, reject) => {
			return new Promise((resolve, reject) => {
				api.post('unit/update', data).then((response) => {
					let data = response.data
					resolve(data)
				}).catch((error) => {
					reject(data)
				})
			})
		})
	},
	ADD_UNIT({commit, state}, data){
		return new Promise((resolve, reject) => {
			api.post('unit/create', data).then((response) => {
				let data = response.data
				resolve(data)
			}).catch((error) => {
				reject(data)
			})
		})
	},
	getUnitDetails({commit,state}, id){
		return new Promise((resolve, reject) => {
			api.get('unit/details/' + id).then((response) => {
				let data = response.data
				resolve(data.data)
			}).catch((error) => {
				reject(error)
			})
		})
	},
	getUnitFilter({commit, state}) {
		api.get('unit/filter',{
			params: {
				owner: state.search.owner,
				apartment: state.search.apartment,
				tower: state.search.tower,
				floor: state.search.floor,
				unit: state.search.unit,
				bed: state.search.bed,
				bath: state.search.bath,
				size: state.search.size,
				status: state.search.status,
				page: state.search.page
			}
		}).then((response) => {
			let data = response.data
			commit('SET_UNIT_FILTER', data.data)
		}).catch((error) => {

		})
	},
	GET_UNIT_PEOPLE({commit, state}, id) {
		return new Promise((resolve, reject) => {
			api.get('unit/people',{
				params: {
					ownerid: id
				}
			}).then((response) => {
				let data = response.data
				resolve(data)
			}).catch((error) => {
				reject(data)
			})
		})
	},
	getListFileUnit({commit, state}) {
		return new Promise((resolve, reject) => {
			api.get('unit/uploaded').then((response) => {
				let data = response.data
				commit('SET_FILE_UNIT', data.data)
			}).catch((error) => {

			})
		})
	},
	uploadFileUnit({commit, state}, file) {
		return new Promise((resolve, reject) => {
			api.post('unit/upload', file).then((response) => {
				let data = response.data
				commit('ADD_FILE_UNIT', data.data)
				resolve(data.data)
			}).catch((error) => {
				reject(error)
			})
		})
	},
	setSearchFilter({commit, state}, search) {
		commit('SET_SEARCH_FILTER',search)
	},
	updateResponseOwner({commit, state}, id, update){
		return new Promise((resolve, reject) => {
			api.post('unit/response/' + id, {...update}).then((response) => {
				// let data = response.data
				// commit('ADD_FILE_UNIT', data.data)
				// resolve(data.data)
			}).catch((error) => {
				// reject(error)
			})
		})
	}
}

const mutations = {
	SET_UNIT_FILTER (state, data) {
		state.units = data
	},
	START_UPLOAD_FILE (state){
		state.upload.progress = true
	},
	SET_FILE_UNIT (state, fileUnit) {
		state.fileUploaded = fileUnit
	},
	ADD_FILE_UNIT (state, newFile) {
		state.upload.progress = false
		state.upload.success = true
		state.fileUploaded = [
			newFile,
			...state.fileUploaded
		]
	},
	SET_SEARCH_FILTER (state, search){
		state.search = {...state.search, ...search}
	}
}

export default {
	state,
	getters,
	actions,
	mutations
}