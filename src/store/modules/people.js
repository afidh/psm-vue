import api from '../../config/api'

const state = {
	peopleList: [],
	responseType: [
	  { key: 'belum_telp', value: 'Belum di Telepon'},
	  { key: 'tidak_di_angkat', value:  'Tidak Di Angkat'},
	  { key: 'salah_sambung', value: 'Salah Sambung'},
	  { key: 'buruk', value: 'Buruk/Rese'},
	  { key: 'terima', value: 'Owner Terima'},
	  { key: 'lengkap', value: 'Data Lengkap'}
	]
}
const getters = {}
const actions = {
	GET_PEOPLE_LIST({commit, state}, data){
		return new Promise((resolve, reject) => {
			api.get('people/list', {
				params: {
					type: data.type,
					page: data.page,
					query: data.query
				}
			}).then((response) => {
				let data = response.data
				commit('SET_PEOPLE_LIST', data.data)
				resolve(data.data)
			}).catch((error) => {
				reject(error)
			})
		})
	},
	UPDATE_PEOPLE_RESPONSE({commit, state}, data){
		return new Promise((resolve, reject) => {
			api.post('people/update/response', data).then((response) => {
				let data = response.data
				commit('UPDATE_PEOPLE_RESPONSE',data.data)
				resolve(data)
			}).catch((error) => {
				reject(data)
			})
		})
	},
	UPDATE_PEOPLE({commit, state}, data){
		return new Promise((resolve, reject) => {
			api.post('people/update', data).then((response) => {
				let data = response.data
				resolve(data)
			}).catch((error) => {
				reject(data)
			})
		})
	}
}
const mutations = {
	SET_PEOPLE_LIST(state, data){
		state.peopleList = {...data}
	},
	UPDATE_PEOPLE_RESPONSE(state, data){

	}
}

export default {
	state,
	getters,
	actions,
	mutations
}