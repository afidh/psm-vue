import api from '../../config/api'

const state = {
	access: localStorage.getItem('access'),
	authenticated: false,
	user: {}
}
const getters = {
	authenticated: (state, getters) => {
		return state.authenticated
	},
	access: (state, getters) => {
		return state.access
	}
}

const actions = {
	LOGIN({commit, state}, auth){
		return new Promise((resolve, reject) => {
			api.post('auth/login', auth).then((response) => {
				let data = response.data
				commit('SET_AUTHENTICATION_USER',data.data)
				resolve(data.data.user)
			}).catch((error) => {
				reject(error)
			})
		})
	},
	INIT_AUTHORIZATION({commit, state}){
		api.get('auth/check').then((response) => {
			let data = response.data
			commit('SET_AUTHENTICATION_USER', data.data)
		}).catch((error) => {
			commit('REVOKE_AUTHENTICATION_USER')
		})
	}
}

const mutations = {
	SET_AUTHENTICATION_USER(state, data){
		state.access = data.access
		state.authenticated = true
		state.user = data.user
		localStorage.setItem('access', state.access)
	},
	REVOKE_AUTHENTICATION_USER(state, data){
		state.access = null
		state.authenticated = false
		state.user = {}
		localStorage.removeItem('access')
	}
}

export default {
	state,
	getters,
	actions,
	mutations
}