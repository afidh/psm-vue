import Vue from 'vue'
import Vuex from 'vuex'
import Unit from './modules/unit'
import Auth from './modules/auth'
import People from './modules/people'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'
export default new Vuex.Store({
  modules: { Unit, Auth, People },
  strict: debug
})