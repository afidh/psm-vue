import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import Login from '../components/pages/auth/login'
import Main from '../components/main'
import PageUnit from '../components/pages/unit/main'
import PageUnitUpload from '../components/pages/unit/unitupload'
import PagePeopleOwner from '../components/pages/people/owner'

const needAuthentication = (to, from, next) => {
	let access = store.getters.access
	console.log('route:access',access)
	if(access){
		next()
		return
	}
	next('/login')
}

const nonAuthentication = (to, from, next) => {
	let access = store.getters.access
	console.log(access)
	console.log('route:nonAuthentication',access)
	if(!access){
		next()
		return
	}
	next('/unit')
}

Vue.use(Router)
export default new Router({
	// mode: 'history',
	routes: [
		{ path: '/', redirect: { name: "login" } },
		{ path: '/login', name: 'login', component: Login, beforeEnter: nonAuthentication },
		{ path: '/dashboard', name: 'main', component: Main, beforeEnter: needAuthentication },
		{ path: '/unit', name: 'unit', component: PageUnit, beforeEnter: needAuthentication },
		{ path: '/unit/upload', name: 'UnitUpload', component: PageUnitUpload, beforeEnter: needAuthentication },
		{ path: '/people/owner', name: 'PeopleOwner', component: PagePeopleOwner, beforeEnter: needAuthentication }
	]
})