import axios from 'axios'
let access = localStorage.getItem('access')

const instance = axios.create({
  baseURL: API_HOST
})

axios.defaults.headers.common['Content-Type'] = 'multipart/form-data'
axios.defaults.headers.common['Authorization'] = access

export default instance